//
//  SpotThumbnailClass.swift
//  MetroSpotDetail
//
//  Created by Nobuhiro on 2014/10/13.
//  Copyright (c) 2014年 Nobuhiro Onishi. All rights reserved.
//

import UIKit

class SpotThumbnailClass: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
    
    var spotData : SpotClass?
    func setSpotData(spot: SpotClass) {
        self.spotData = spot
    }
}
