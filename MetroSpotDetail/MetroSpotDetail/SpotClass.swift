//
//  SpotClass.swift
//  MetroSpotDetail
//
//  Created by Nobuhiro on 2014/10/13.
//  Copyright (c) 2014年 Nobuhiro Onishi. All rights reserved.
//

import Foundation

class SpotClass {
    var spotId : Int
    var description = ""
    var likeNum = 0
    var goneNum = 0
    var likeFlg = false
    var goneFlg = false
    var spotThumbUrl = ""
    var spotImageUrl = ""
    
    
    init(spotId: Int, title: String, description: String, likeNum: Int, goneNum: Int, likeFlg: Bool, goneFlg: Bool, spotThumbUrl: String, spotImageUrl: String) {
        self.spotId = spotId
        self.description = description
        self.likeNum = likeNum
        self.goneNum = goneNum
        self.likeFlg = likeFlg
        self.spotThumbUrl = spotThumbUrl
        self.spotThumbUrl = spotThumbUrl
        self.spotImageUrl = spotImageUrl
    }
}