//
//  ViewController.swift
//  MetroSpotDetail
//
//  Created by Nobuhiro on 2014/10/13.
//  Copyright (c) 2014年 Nobuhiro Onishi. All rights reserved.
//

import UIKit

class SpotMapViewController: UIViewController {
    // ----- for mock spot data
    @IBOutlet weak var spotThumb1: SpotThumbnailClass!
    @IBOutlet weak var spotThumb2: SpotThumbnailClass!
    
    // ----- for popup spot detail view
    @IBOutlet weak var popupSpotDetailView: UIView!
    @IBOutlet weak var spotImageView: UIImageView!
    @IBOutlet weak var likeIconImageView: UIImageView!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var likeNumLabel: UILabel!
    @IBOutlet weak var goneIconImageView: UIImageView!
    @IBOutlet weak var goneButton: UIButton!
    @IBOutlet weak var goneNumLabel: UILabel!
    @IBOutlet weak var spotDescriptionTextView: UITextView!
    
    @IBOutlet weak var getImageDataIndicator: UIActivityIndicatorView!
    
    // spot datq
    var selectedSpot : SpotClass?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setMockData()
        popupSpotDetailView.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func spotThumb1Pushed(sender: AnyObject) {
        selectedSpot = spotThumb1!.spotData
        showSpotDetail()
    }

    @IBAction func spotThumb2Pushed(sender: AnyObject) {
        selectedSpot = spotThumb2!.spotData
        showSpotDetail()
    }
    
    // ----- for popup spot detail view
    //TODO
    // スポット詳細ビュー「いいね」ボタン押下時
    @IBAction func likeButtonPushed(sender: AnyObject) {
        var beforeLikeNum = selectedSpot!.likeNum
        if (selectedSpot!.likeFlg) {
            // Update From : true,  To : false
            selectedSpot!.likeFlg = false
            likeIconImageView.image = UIImage(named: "icon_like_off.png")
            likeButton.setTitle("いいね！", forState: UIControlState.Normal)
            selectedSpot!.likeNum = beforeLikeNum > 0 ? --beforeLikeNum : 0
            likeNumLabel.text = String(selectedSpot!.likeNum)
        } else {
            // Update From : false,  To : true
            selectedSpot!.likeFlg = true
            likeIconImageView.image = UIImage(named: "icon_like_on.png")
            likeButton.setTitle("取り消す", forState: UIControlState.Normal)
            selectedSpot!.likeNum = ++beforeLikeNum
            likeNumLabel.text = String(selectedSpot!.likeNum)
        }
    }
    
    //TODO
    // スポット詳細ビュー「いったよ」ボタン押下時
    @IBAction func goneButtonPushed(sender: AnyObject) {
        var beforeGoneNum = selectedSpot!.goneNum
        if (selectedSpot!.goneFlg) {
            // Update From : true,  To : false
            selectedSpot!.goneFlg = false
            goneIconImageView.image = UIImage(named: "icon_gone_off.png")
            goneButton.setTitle("いったよ", forState: UIControlState.Normal)
            selectedSpot!.goneNum = beforeGoneNum > 0 ? --beforeGoneNum : 0
            goneNumLabel.text = String(selectedSpot!.goneNum)
        } else {
            // Update From : false,  To : true
            selectedSpot!.goneFlg = true
            goneIconImageView.image = UIImage(named: "icon_gone_on.png")
            goneButton.setTitle("取り消す", forState: UIControlState.Normal)
            selectedSpot!.goneNum = ++beforeGoneNum
            goneNumLabel.text = String(selectedSpot!.goneNum)
        }
    }
    
    // スポット詳細ビュー「ここに行く」ボタン押下時
    @IBAction func goSpotButtonPushed(sender: AnyObject) {
        println("go spot button pushed !!!")
        return
    }
    
    // スポット詳細ビュー「閉じる」ボタン押下時
    @IBAction func closeSpotDetailButtonPushed(sender: AnyObject) {
        popupSpotDetailView.hidden = true
    }
    
    // スポット詳細ビューの表示（アニメーション）
    func showSpotDetail() {
        if (selectedSpot == nil) {
            return
        } else {
            setSpotData()
            popupSpotDetailView.hidden = false
            popupSpotDetailView.alpha = 0.0
            
            UIView.animateWithDuration(
                0.4,
                delay: 0.0,
                options: UIViewAnimationOptions.CurveEaseIn,
                animations: {
                    self.popupSpotDetailView.alpha = 1.0;
                },
                completion: {
                    (value: Bool) in
                }
                
            );
        }
    }
    
    // スポット詳細データを詳細ビューにセットする
    func setSpotData() {
        // 画像データリセット
        spotImageView.image = nil
        
        // 画像サーバからスポット画像をダウンロード
        getImageDataIndicator.startAnimating()
        var req = NSURLRequest(URL: NSURL(string: selectedSpot!.spotImageUrl))
        NSURLConnection.sendAsynchronousRequest(req, queue: NSOperationQueue.mainQueue(), completionHandler: self.getSpotImageHttp)
        
        // like button
        likeIconImageView.image = selectedSpot!.likeFlg == true ? UIImage(named: "icon_like_on.png") : UIImage(named: "icon_like_off.png")
        likeNumLabel.text = String(selectedSpot!.likeNum)
        
        // gone button
        goneIconImageView.image = selectedSpot!.goneFlg == true ? UIImage(named: "icon_gone_on.png") : UIImage(named: "icon_gone_off.png")
        goneNumLabel.text = String(selectedSpot!.goneNum)
        
        // description
        if (selectedSpot!.description == "") {
            spotDescriptionTextView.text = "no message"
        } else {
            spotDescriptionTextView.text = selectedSpot!.description
        }
        spotDescriptionTextView.editable = false
        
        //TODO 「いったよ」ボタンが有効かどうかチェック
        if (!checkGoneButtonEnabled()) {
            goneButton.enabled = false;
        }
        
        
    }
    
    // 画像データ取得終了後に呼ばれるメソッド
    func getSpotImageHttp(res: NSURLResponse?, data: NSData?, error: NSError?) {
        getImageDataIndicator.stopAnimating()
        if (data == nil) {
            spotImageView.image = UIImage(named: "no_image.png")
        } else {
            var imageData : UIImage = UIImage(data: data!)
            spotImageView.image = imageData
        }
    }
    
    //TODO
    func checkGoneButtonEnabled() -> Bool {
        return true
    }
    
    // for setup mock data
    // this is temporary data and this function won't be necessary
    func setMockData() {
        var spot1 = SpotClass(
                spotId: 1,
                title: "spot1 title",
                description: "",
                likeNum: 1,
                goneNum: 10,
                likeFlg: false,
                goneFlg: false,
                spotThumbUrl: "thumb_sample0.jpg",
                spotImageUrl: "http://www14351ue.sakura.ne.jp/sanpo/images/sample00_300.jpg")
        
        var spot2 = SpotClass(
                spotId: 2,
                title: "spot2 title",
                description: "サンプルスポットデータその2のコメントを表示しています。",
                likeNum: 2,
                goneNum: 20,
                likeFlg: false,
                goneFlg: false,
                spotThumbUrl: "thumb_sample1.jpg",
                spotImageUrl: "http://www14351ue.sakura.ne.jp/sanpo/images/sample01_300.jpg")
        
        spotThumb1.setSpotData(spot1)
        spotThumb2.setSpotData(spot2)
        
        spotThumb1.setTitle("", forState: UIControlState.Normal)
        spotThumb2.setTitle("", forState: UIControlState.Normal)
        spotThumb1.setBackgroundImage(UIImage(named: spot1.spotThumbUrl), forState: UIControlState.Normal)
        spotThumb2.setBackgroundImage(UIImage(named: spot2.spotThumbUrl), forState: UIControlState.Normal)
    }
}

